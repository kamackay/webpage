import {
  createStyles,
  makeStyles,
  Paper,
  Theme,
  Typography,
} from "@material-ui/core";
import classNames from "classnames";
import * as React from "react";

const styles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing(3, 2),
      height: "95vh",
    },
    paragraph: {},
  })
);

export default function Page() {
  const classes = styles();
  const params = new URLSearchParams(window.location.pathname);
  const name = params.get("name");

  return (
    <div className={classNames("container", "container-fluid")}>
      <Paper className={classes.root}>
        <Typography variant="h2" component="h2">
          You got Phished{!!name && `, ${name}`}
        </Typography>
        <p className={classes.paragraph} style={{ fontSize: 15 }}>
          You were phished, and Keith is probably going to lecture you, and it's
          gonna be annoying as hell.
        </p>
      </Paper>
    </div>
  );
}
